# :material-cube: Task templates overview

This is the documentation project of [Task Template](https://gitlab.com/op_so/task/task-templates), a set of reusable [Task](https://taskfile.dev/) templates, easy to install and ready to use. Go Task is a popular task runner written in Go, known for its simplicity and ease of use. These templates aim to streamline and simplify common development or devops tasks.

!!! info "Key Features"

    * **Pre-defined tasks:** The project offers a variety of pre-defined tasks for common operations, such as:

        * Linting code
        * Products wrappers (Ansible, Multipass ...)
        * Encrypting/decrypting tasks
        * Addons (Robot Framework, Docker ...)
        * SBOM tools
        * Version extract and format

    * **Customization:** While templates provide a starting point, you can customize them to fit your specific project needs.

    * **Easy Integration:** These templates are designed to integrate seamlessly with Go Task's workflow.

## Benefits

* **Improved productivity:** Pre-defined tasks save time and effort compared to writing them from scratch.
* **Consistency:** Enforces consistency in project workflows across developers.
* **Reduced complexity:** Simplifies task management for complex projects.
* **Standardize execution:** Same tasks are executed in local and CI.

## Additional notes

The project leverages Docker containers for some tasks, so ensure Docker is installed and configured.
You can extend the existing templates or create your own to suit your specific workflow.

## Open source

### License

These task templates are available under an open-source license mainly [`MIT`](https://opensource.org/license/MIT). This means they're accessible and usable by anyone for various purposes:

* **Free to use:** You can integrate these templates into your projects without any licensing fees. Whether your project is commercial or non-commercial, you're free to leverage their functionality.
* **Adaptable to your needs:** Feel free to modify and customize the templates to align with your specific project requirements. You can add, remove, or adjust tasks to streamline your development workflow.
* **Shareable:** The open-source nature allows you to redistribute the original or modified templates to others.
* **License and copyright notice:** When distributing the templates (or modified versions), it's usually necessary to include a copy of the MIT License and a copyright notice acknowledging the original creators.

### Contributions

Contributions are welcome:

* **Report issues:** If you encounter problems with the templates or have suggestions for improvement, you can raise them by opening [issues](https://gitlab.com/op_so/task/task-templates/-/issues) on the [GitLab repository](https://gitlab.com/op_so/task/task-templates).
* **Contribute to new templates:** Do you have unique task requirements? Consider creating new templates and submitting them as [merge requests](https://gitlab.com/op_so/task/task-templates/-/merge_requests). This expands the available functionalities and benefits the entire community.
* **Improve existing templates:** You can propose modifications to existing templates to address potential shortcomings or suggest clarity improvements with [issues](https://gitlab.com/op_so/task/task-templates/-/issues) or [merge requests](https://gitlab.com/op_so/task/task-templates/-/merge_requests).

The [developer manual](developer_manual.md) can help to code a template with some tips and rules.

## To go further

* [Task presentation](what_is_task.md)
* [Task official documentation](https://taskfile.dev/)
* [Getting started](getting_started.md)
* [Tasks templates list](task_templates/index.md)
* [Developer manual](developer_manual.md)
* [Task templates repository](https://gitlab.com/op_so/task/task-templates)
