# :simple-task: What is task?

## Introduction

For full documentation visit [taskfile.dev](https://taskfile.dev/).

Go Task is a popular task runner tool written in Go, known for its simplicity and ease of install and use.
Go Task uses a YAML file named `Taskfile.yml` to define your build tasks. This file resides in the root directory of your project.
A basic task definition looks like this:

``` yaml title="Taskfile.yml"
version: '3'  # Specify Taskfile version

tasks:
  hello:  # Task name
    cmds:  # Commands to execute
      - echo "Hello, world!"
```

Once you have defined your tasks, you can run them using the task command followed by the task name:

``` shell
task hello
```

This will execute the commands defined in the `hello` task and prints `Hello, world!`.

## A replacement of the GNU Make build tool

!!! info "Features:"

    * **Dependencies:** You can specify dependencies between tasks using the `deps` key in the task definition.
    * **Conditional execution:** Different checks context `status`, `preconditions`, `requires` to conditionally run tasks.
    * **Variables:** Define and use variables within your tasks.
    * **Custom Build Commands:** Your commands are really in shell.
    * **Prevent unnecessary work:** By fingerprinting locally generated files and their sources

Example:

``` yaml title="Taskfile.yml"
version: '3'

tasks:
  build:
    desc: "Build Go application"
    deps: [js]
    cmds:
      - go build -v -i main.go

  js:
    desc: "Build Javascript assets"
    cmds:
      - esbuild --bundle --minify js/index.js > public/bundle.js
    sources:
      - src/js/**/*.js
    generates:
      - public/bundle.js
```

## A simple task runner as a local CI

An easy way to simplify complex workflows with simple tasks.
An example to execute acceptance tests (e2e) with prerequisites:

``` yaml title="Taskfile.yml"
test-e2e:
    desc: "Run end to end tests with Robot Framework"
    cmds:
      - docker compose up
      - defer: docker compose down
      - cat ./dump-e2e.sql | docker exec -t container-psql psql -U $POSTGRES_USER -p $POSTGRES_PASSWORD -d $POSTGRES_DB
      - robot --include e2e tests/
    env:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      POSTGRES_DB: postgres
      ...
```

## Modularization and reusability

Go Task allows you to define reusable tasks that can be used in different contexts:

* This eliminates the need to repeat **lengthy commands** across multiple scripts or files.
* You can create task libraries that encapsulate common functionalities, promoting **code reuse and reducing redundancy** with simple input data validation.
