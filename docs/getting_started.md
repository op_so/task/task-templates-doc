# :material-school: Getting started

## Requirements

!!! info "Pre-requisites"

    * :material-cube: [Task](https://taskfile.dev)
    * :material-web: `curl` or `wget`
    * :material-docker: [Docker](https://docs.docker.com/engine/install/) (recommended): Many templates use Docker images to avoid installing software.

## Structure

``` shell
.
├─ Taskfile.d/
│  ├─ ...
│  ├─ git.yml
│  ├─ lint.yml
│  └─ ...
├─ Taskfile.project.yml
└─ Taskfile.yml
```

!!! Notes

    * `Taskfile.d`: Directory of the task template files
    * `Taskfile.project.yml`: Task file for the project that has the `TASK_TEMPLATES` variable
    * `Taskfile.yml`: Core Task file with generic tasks, don't edit it!

## Installation

Download the main `Taskfile.yml` and the project file template `Taskfile.project.yml`:

with `curl`:

``` shell  linenums="1"
curl --progress-bar -o Taskfile.yml https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.dist.yml
curl --progress-bar -o Taskfile.project.yml https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.project.dist.yml
```

or with `wget`:

``` shell  linenums="1"
wget -cq --show-progress -O Taskfile.yml https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.dist.yml
wget -cq --show-progress -O Taskfile.project.yml https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.project.dist.yml
```

!!! warning

    On Alpine Linux, `--show-progress` option isn't available.

## Setup

Select your templates by editing the `Taskfile.project.yml` file variable `TASK_TEMPLATES`:

example:

``` yaml title="Taskfile.project.yml"  linenums="10"
vars:
  TASK_TEMPLATES: git,lint
```

and run:

``` { .shell .copy }
task install-templates
```

A specific version of a template can be specify as follow:

example for git version `1.7.3`:

``` yaml title="Taskfile.project.yml" linenums="10"
vars:
  TASK_TEMPLATES: git[1.7.3],lint
```

!!! note "Git configuration (Optional)"

    `Taskfile.project.yml` is the file that has your specific project tasks. You should probably commit it.
    If you always want the last version of the task templates, add this following line in your `.gitignore` file

    ``` text title=".gitignore"
    /Taskfile.d/
    ```

    Otherwise, if you prefer stability you should also commit the content of the `Taskfile.d` directory or specify the versions of the templates.

## Usage

* `task` command without any parameter shows the available installed tasks:

![Available tasks](images/tasks-list.png "Available tasks")

* Example of usage with the `lint`template:

``` shell
❯ task lint:pre-commit
Using default tag: latest
latest: Pulling from jfxs/pre-commit
Digest: sha256:5bd813b076fed1fb566e59e163d27e382df599822fb1b187eb6020402b9351e5
Status: Image is up to date for jfxs/pre-commit:latest
docker.io/jfxs/pre-commit:latest
trim trailing whitespace.................................................Passed
fix end of files.........................................................Passed
check yaml...............................................................Passed
check for added large files..............................................Passed
check that executables have shebangs.................(no files to check)Skipped
check json...........................................(no files to check)Skipped
check for merge conflicts................................................Passed
check that scripts with shebangs are executable..........................Passed
check for broken symlinks............................(no files to check)Skipped
check toml...............................................................Passed
check xml................................................................Passed
detect private key.......................................................Passed
fix end of files.........................................................Passed
fix utf-8 byte order marker..............................................Passed
mixed line ending........................................................Passed
trim trailing whitespace.................................................Passed
```

* Example of usage with the project task `20-generate-doc` defined as follow:

``` yaml title="Taskfile.project.yml"  linenums="56"
  20-generate-doc:
    desc: "[PROJECT] Generate doc with task2md."
    cmds:
      - date > {{.FILE_TASK_START}}
      - defer: rm -f {{.FILE_TASK_START}}
      - mkdir -p {{.TASK_TEMPLATES_DOC_DIR}}
      - docker run -t -v $(pwd):/src python:slim-bookworm /bin/bash -c "pip3 install task2md && task2md dir -i /src/{{.TASK_TEMPLATES_DOWNLOAD_DIR}} -d /src/{{.TASK_TEMPLATES_DOC_DIR}}"
      - cp -f mkdocs-ref.yml mkdocs.yml
      - |
        md_files=$(cd docs && find task_templates -name "*.md" ! -name "index.md" | sort)
        for file in $md_files; do
          echo "    - $file" >> mkdocs.yml
        done
      - echo "" && echo "Checks Start $(cat {{.FILE_TASK_START}}) - End $(date)"
    silent: true
```

``` shell
❯ task 00:20-generate-doc
Collecting task2md
  Downloading task2md-1.1.2-py3-none-any.whl.metadata (3.7 kB)
Collecting click>=8.1.7 (from task2md)
  Downloading click-8.1.7-py3-none-any.whl.metadata (3.0 kB)
Collecting pydantic>=2.6.3 (from task2md)
...

Task documentation generated: lint.md
Task documentation generated: version.md
Task documentation generated: git.md
Index documentation generated: index.md

Checks Start Jeu 25 avr 2024 20:13:11 CEST - End Jeu 25 avr 2024 20:13:15 CEST
```

## Upgrade

* To upgrade your existing templates, just run the command:

``` { .shell .copy }
task install-templates
```

* To add a new template, add it to the variable `TASK_TEMPLATES` and run `task install-templates`.

``` { .shell .copy }
task install-templates
```
