# :material-laptop: Developer manual

## Reference documentation

* [Task official documentation](https://taskfile.dev/)
* [Task style guide](https://taskfile.dev/styleguide/)

## Task example

``` yaml title="version.yml"  linenums="174"
  get-latest-github:                                                 # (1)
    desc: "Get latest version from Github: REPO|R=go-task/task (*)"  # (2)
    summary: |                                                       # (3)
      [VERSION] Get latest version from Github:                      # (3.1)
      Usage: task version:get-latest-github REPO|R=<owner>/<repo>

      Arguments:
        REPO | R: Repository in format: <owner>/<repo> (required)    # (3.2)

      Requirements:                                                  # (3.3)
        - curl or wget
        - jq
    vars:
      REPO: '{{.REPO | default .R}}'                                 # (4)
      CURRENT_DATE:                                                  # (5)
        sh: date -u +'%Y-%m-%dT%H:%M:%SZ'
    cmds:
      - echo "{{.CURRENT_DATE}}"                                     # (6)
      - |                                                            # (7)
        if [ -x "$(command -v curl)" ]; then
          curl -sL https://api.github.com/repos/{{.REPO}}/releases/latest | jq -r ".tag_name" | sed 's/v//g'
        else
          wget -qO- https://api.github.com/repos/{{.REPO}}/releases/latest | jq -r ".tag_name" | sed 's/v//g'
        fi
    preconditions:
      - sh: command -v curl || command -v wget
        msg: "curl or wget are not installed"
      - sh: command -v jq
        msg: "jq is not installed"
      - sh: test -n "{{.REPO}}"                                      # (8)
        msg: "REPO|R argument is required"
    silent: true                                                     # (9)
```

| # (n) | Description |
| ----- | ----------- |
| # (1) | Name of the task. [Use kebab case for task names](https://taskfile.dev/styleguide/#use-kebab-case-for-task-names) |
| # (2) | `desc:` Description of the task, visible when you enter the command `task` without any parameter |
| # (3) | `summary:` Details of the task, visible when you enter the command `task usage TSK=<template:task_name>` |
| # (3.1) | Short description of the CLI usage |
| # (3.2) | List of arguments. Separator `:` |
| # (3.3) | Requirements if any |
| # (4) | Variable for long and short argument usage, declaration example with a default value: `DIR: '{{.DIR \| default .D \| default "."}}'` |
| # (5) | Variable set with a one line shell command |
| # (6) | A one line shell command |
| # (7) | A multi lines shell commands |
| # (8) | Test for required argument |
| # (9) | Disable verbose mode |

With these details, the render of the documentation is available [here](https://task-templates.pages.dev/task_templates/version/#versionget-latest-github)

## Useful tips

* Like `make` a shell context is created for every command lines (`cmds:`)

``` yaml
    cmds:
      - current_date=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
      - echo "$current_date"  # it does not work!
```

The solution is to set the variable directly like previous (#5) or use a multi-line command:

``` yaml
    cmds:
      - |
        current_date=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
        echo "$current_date"
```

* Avoid long script block (multi-line command) if it's possible. Lint and debug aren't so easy.

## Documentation generation

The documentation is generated automatically from the Task files with the [`task2md`](https://gitlab.com/op_so/task/task2md) script.

### Metadata

A list of metadata can be defined at the beginning of the file.
Example:

``` yaml title="version.yml"  linenums="1"
---
# https://taskfile.dev
# @description: A set of tasks to lint different types of files.
# @tags: lint, docker, CI, CLI
...
```

| Metadata | Description |
| --------- | ----------- |
| `# @description:` | General description of the template |
| `# @tags:` | List of tags separated by comma |
| `# @authors:` | List of authors separated by comma (not yet implemented) |
| `# @file-raw:` | Raw template file |
| `# @file-ui:` | User friendly template file |
| `# @home:` | Home of the template file project |
| `# @links:` | Related links in Markdown format (not yet implemented) |
| `# @license:` | License of the template |
| `# @status:` | Status of the template. Available values: `draft` (default), `beta`, `stable`, `deprecated` |
| `# @deprecated-tasks:` | List of deprecated tasks separated by comma (not yet implemented) |

### Global variables

``` yaml title="git.yml"  linenums="14"
version: '3'

vars:
  IMAGE_DEFAULT: jfxs/commitizen-semantic-release:latest  # Default image
  DOCKER_EXEC_OPTS: ""  # Optional Docker options for docker exec command
  DOCKER_RUN_OPTS: ""  # Optional Docker options for docker run command

tasks:
   ...
```

The global variables are a set of variables to apply to all tasks of the Taskfile.
A description can be set in comment (2 spaces and `#`) for the documentation and renders as follow:

| Variables | Description | Default value |
| --------- | ----------- | ------------- |
| `DOCKER_EXEC_OPTS` | Optional Docker options for docker exec command |  -  |
| `DOCKER_RUN_OPTS` | Optional Docker options for docker run command |  -  |
| `IMAGE_DEFAULT` | Default image | `jfxs/commitizen-semantic-release:latest` |
