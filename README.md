# Task templates documentation

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![Built with Material for MkDocs](https://img.shields.io/badge/Material_for_MkDocs-526CFE?style=for-the-badge&logo=MaterialForMkDocs&logoColor=white)](https://squidfunk.github.io/mkdocs-material/)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/task/task-templates-doc?style=for-the-badge)](https://gitlab.com/op_so/task/task-templates-doc/pipelines)

Project to generate the [Task templates documentation](https://task-templates.pages.dev/) with [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

## Authors

<!-- vale off -->
- **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
